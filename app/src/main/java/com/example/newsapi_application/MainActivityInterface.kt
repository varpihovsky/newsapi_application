package com.example.newsapi_application

interface MainActivityInterface {
    fun switchToNews()

    fun switchToMap()
}