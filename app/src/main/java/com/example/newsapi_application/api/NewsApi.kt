package com.example.newsapi_application.api

import com.example.newsapi_application.model.NewsResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface NewsApi {
    @GET("everything")
    fun getNews(
        @Query("q") keyWords: String,
        @Query("apiKey") apiKey: String
    ): Call<NewsResponse>
}