package com.example.newsapi_application

import android.Manifest
import android.os.Bundle
import android.os.PersistableBundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.commit
import com.example.newsapi_application.databinding.ActivityMainBinding
import com.example.newsapi_application.map.MapFragment
import com.example.newsapi_application.news.NewsFragment
import com.mapbox.search.MapboxSearchSdk
import com.mapbox.search.location.DefaultLocationProvider
import pub.devrel.easypermissions.EasyPermissions


class MainActivity : AppCompatActivity(), MainActivityInterface,
    EasyPermissions.PermissionCallbacks {
    private lateinit var binding: ActivityMainBinding
    private val mainActivityPresenter = MainActivityPresenter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        setSupportActionBar(binding.toolbar)

        if (savedInstanceState == null) {
            MapboxSearchSdk.initialize(
                application,
                getString(R.string.mapbox_access_token),
                DefaultLocationProvider(application)
            )
            supportFragmentManager.commit {
                add(binding.newsFragment.id, NewsFragment.newInstance(), NEWS_FRAGMENT_TAG)
            }
        }
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        savedInstanceState.getParcelable<MainActivityPresenter.CurrentScreen>(
            CURRENT_SCREEN_PARAM
        )?.let { mainActivityPresenter.onActivityRecreate(it) }
    }

    override fun onSaveInstanceState(outState: Bundle, outPersistentState: PersistableBundle) {
        super.onSaveInstanceState(outState, outPersistentState)
        outState.putParcelable(CURRENT_SCREEN_PARAM, mainActivityPresenter.getCurrentScreen())
    }

    override fun onDestroy() {
        mainActivityPresenter.unbind()
        super.onDestroy()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.toolbar_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        R.id.action_switch_to_map -> {
            mainActivityPresenter.onMapActionClicked()
            true
        }
        R.id.action_switch_to_news -> {
            mainActivityPresenter.onNewsActionClicked()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    override fun onPermissionsDenied(requestCode: Int, perms: MutableList<String>) {
        if (isListContainsMapPermissions(perms)) {
            Toast.makeText(
                this,
                "Cant load location. Permissions was not granted",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    override fun onPermissionsGranted(requestCode: Int, perms: MutableList<String>) {
        if (isListContainsMapPermissions(perms)) {
            (supportFragmentManager.findFragmentByTag(MAP_FRAGMENT_TAG) as? MapFragment)?.notifyPermissionsGranted()
        }
    }

    private fun isListContainsMapPermissions(permissions: List<String>) =
        permissions.contains(Manifest.permission.ACCESS_COARSE_LOCATION) && permissions.contains(
            Manifest.permission.ACCESS_FINE_LOCATION
        )

    override fun switchToNews() {
        supportFragmentManager.commit {
            replace(binding.newsFragment.id, NewsFragment(), NEWS_FRAGMENT_TAG)
        }
    }

    override fun switchToMap() {
        supportFragmentManager.commit {
            replace(binding.newsFragment.id, MapFragment(), MAP_FRAGMENT_TAG)
        }
    }

    companion object {
        private const val NEWS_FRAGMENT_TAG = "news"
        private const val MAP_FRAGMENT_TAG = "map"
        private const val CURRENT_SCREEN_PARAM = "first_start"
    }
}