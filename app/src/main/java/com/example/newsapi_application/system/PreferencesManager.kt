package com.example.newsapi_application.system

import android.app.Activity
import android.content.Context

class PreferencesManager(private val activity: Activity?) {
    fun saveString(key: String, value: String) {
        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE) ?: return
        with(sharedPref.edit()) {
            putString(key, value)
            apply()
        }
    }

    fun loadString(key: String, defaultValue: String?): String? {
        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE) ?: return null
        return sharedPref.getString(key, defaultValue)
    }
}