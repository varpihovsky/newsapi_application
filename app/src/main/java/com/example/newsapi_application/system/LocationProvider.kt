package com.example.newsapi_application.system

import android.annotation.SuppressLint
import android.content.Context
import android.location.Location
import com.google.android.gms.location.LocationServices
import javax.inject.Inject

class LocationProvider @Inject constructor(context: Context) {
    private val fusedLocationClient = LocationServices.getFusedLocationProviderClient(context)

    @SuppressLint("MissingPermission")
    fun getCurrentLocationCallback(block: (Location) -> Unit) {
        val lastLocation = fusedLocationClient.lastLocation
        lastLocation.addOnSuccessListener { successLocation ->
            block.invoke(successLocation)
        }
    }
}