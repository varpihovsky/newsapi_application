package com.example.newsapi_application.system

class NoInternetConnectionException(override val message: String? = "") :
    Exception()