package com.example.newsapi_application.system

class ViewModelEvent<T>(private val data: T) {
    private var isTaken = false

    fun get(): T? =
        if (isTaken) {
            null
        } else {
            isTaken = true
            data
        }
}