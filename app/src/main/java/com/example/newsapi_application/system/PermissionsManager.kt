package com.example.newsapi_application.system

import android.app.Activity
import android.content.pm.PackageManager
import pub.devrel.easypermissions.EasyPermissions
import pub.devrel.easypermissions.PermissionRequest
import javax.inject.Inject

class PermissionsManager @Inject constructor(private val activity: Activity) {
    fun requestPermission(vararg permissions: String) {
        EasyPermissions.requestPermissions(
            PermissionRequest.Builder(
                activity,
                PackageManager.PERMISSION_GRANTED,
                *permissions
            ).build()
        )
    }
}