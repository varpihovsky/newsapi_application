package com.example.newsapi_application

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

class MainActivityPresenter(private var mainActivityInterface: MainActivityInterface?) {
    @Parcelize
    enum class CurrentScreen : Parcelable { MAP, NEWS }

    private var currentScreen: CurrentScreen = CurrentScreen.NEWS

    fun onActivityRecreate(currentScreen: CurrentScreen) {
        when (currentScreen) {
            CurrentScreen.NEWS -> mainActivityInterface?.switchToNews()
            CurrentScreen.MAP -> mainActivityInterface?.switchToMap()
        }
    }

    fun getCurrentScreen(): CurrentScreen {
        return currentScreen
    }

    fun onMapActionClicked() {
        mainActivityInterface?.switchToMap()
        currentScreen = CurrentScreen.MAP
    }

    fun onNewsActionClicked() {
        mainActivityInterface?.switchToNews()
        currentScreen = CurrentScreen.NEWS
    }

    fun unbind() {
        mainActivityInterface = null
    }
}