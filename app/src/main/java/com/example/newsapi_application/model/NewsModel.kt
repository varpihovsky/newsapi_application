package com.example.newsapi_application.model

import com.example.newsapi_application.api.NewsApi
import com.example.newsapi_application.system.ConnectionManager
import com.example.newsapi_application.system.NoInternetConnectionException
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class NewsModel {
    private val retrofit: Retrofit =
        Retrofit.Builder()
            .baseUrl(NEWS_API_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

    private val newsApi = retrofit.create(NewsApi::class.java)

    fun getData(request: String): MutableList<Article>? {
        if (!ConnectionManager().isConnected()) {
            throw NoInternetConnectionException()
        }

        return newsApi.getNews(request, API_KEY).execute()
            .body()?.articles?.onEach {
                it.publishedAt?.length?.let { length ->
                    if (length >= DATE_STRING_LAST_SYMBOL_INDEX) {
                        it.publishedAt = it.publishedAt?.substring(
                            DATE_STRING_FIRST_SYMBOL_INDEX,
                            DATE_STRING_LAST_SYMBOL_INDEX
                        )
                    }
                }
            }?.apply { sortedBy { it.publishedAt } }
    }

    companion object {
        private const val API_KEY = "1f26f91da6624b68bb50a96030921343"
        private const val NEWS_API_URL = "https://newsapi.org/v2/"

        private const val DATE_STRING_LAST_SYMBOL_INDEX = 10
        private const val DATE_STRING_FIRST_SYMBOL_INDEX = 0
    }
}