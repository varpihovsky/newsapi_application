package com.example.newsapi_application.news.widgets

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import com.example.newsapi_application.R
import com.example.newsapi_application.databinding.ConfirmationDialogBinding

class ConfirmationDialog : DialogFragment() {
    private lateinit var binding: ConfirmationDialogBinding
    private var confirmationDialogCallback: ConfirmationDialogCallback? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.confirmation_dialog, container, false)

        arguments?.run {
            getString(TITLE_PARAM)?.let { dialog?.setTitle(it) }
            getString(DESCRIPTION_PARAM)?.let { binding.confirmationDialogDescription.text = it }
            getString(ACCEPT_BUTTON_TEXT_PARAM)?.let {
                binding.confirmationDialogAcceptButton.text = it
            }
            getString(DECLINE_BUTTON_TEXT_PARAM)?.let {
                binding.confirmationDialogDeclineButton.text = it
            }
        }

        binding.confirmationDialogAcceptButton.setOnClickListener { confirmationDialogCallback?.onConfirmButtonClicked() }
        binding.confirmationDialogDeclineButton.setOnClickListener { confirmationDialogCallback?.onDeclineButtonClicked() }

        return binding.root
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (targetFragment is ConfirmationDialogCallback) {
            confirmationDialogCallback = targetFragment as ConfirmationDialogCallback
        } else {
            throw UnsupportedOperationException("targetFragment should implement ConfirmationDialog interface")
        }
    }

    interface ConfirmationDialogCallback {
        fun onConfirmButtonClicked()
        fun onDeclineButtonClicked()
    }

    companion object {
        private const val TITLE_PARAM = "title"
        private const val DESCRIPTION_PARAM = "description"
        private const val ACCEPT_BUTTON_TEXT_PARAM = "accept"
        private const val DECLINE_BUTTON_TEXT_PARAM = "decline"

        fun newInstance(
            titleText: String? = null,
            descriptionText: String? = null,
            acceptButtonText: String? = null,
            declineButtonText: String? = null
        ): ConfirmationDialog {
            val dialog = ConfirmationDialog()
            dialog.arguments = Bundle().apply {
                putString(TITLE_PARAM, titleText)
                putString(DESCRIPTION_PARAM, descriptionText)
                putString(ACCEPT_BUTTON_TEXT_PARAM, acceptButtonText)
                putString(DECLINE_BUTTON_TEXT_PARAM, declineButtonText)
            }
            return dialog
        }
    }

}