package com.example.newsapi_application.news.widgets

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.newsapi_application.R
import com.example.newsapi_application.databinding.NewsItemBinding
import com.example.newsapi_application.model.Article


class RecyclerViewAdapter(
    var dataList: MutableList<Article>,
    private val removeButtonClickListener: (item: Int) -> Unit
) :
    RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>() {

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val binding = DataBindingUtil.bind<NewsItemBinding>(itemView)
        val imageButtonDelete = binding?.imageButtonDelete
        val newsTitle = binding?.newsTitle
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.news_item, parent, false)
        )


    override fun getItemCount(): Int = dataList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding?.article = dataList[position]
        holder.imageButtonDelete?.setOnClickListener {
            removeButtonClickListener.invoke(holder.adapterPosition)
        }

        holder.newsTitle?.title = holder.binding?.article?.title
        holder.newsTitle?.urlToImage = holder.binding?.article?.urlToImage
    }
}