package com.example.newsapi_application.news

import android.util.Log
import com.example.newsapi_application.R
import com.example.newsapi_application.model.Article
import com.example.newsapi_application.model.NewsModel
import kotlinx.coroutines.*

class NewsPresenter(private var newsActivityInterface: NewsFragmentInterface?) {
    private val newsModel = NewsModel()
    private var lastRequest: String? = null
    private var articleRemovePosition: Int? = null

    fun onViewCreated() {
        val request = newsActivityInterface?.loadRequest()
        if (request != NULL_REQUEST) {
            runBlocking { doRequest(request) }
        }
    }

    private suspend fun doRequest(request: String) {
        val result = GlobalScope.async(Dispatchers.IO) { processRequest(request) }
        if (result.await()) {
            newsActivityInterface?.showTextInInputField(request)
        }
    }


    fun onFindButtonClick(request: String) {
        newsActivityInterface?.showLoading()
        newsActivityInterface?.hideKeyboard()

        GlobalScope.launch(Dispatchers.IO) { processRequest(request) }
    }

    private fun processRequest(request: String): Boolean {
        lastRequest = request

        try {
            val articleList = newsModel.getData(request)

            GlobalScope.launch(Dispatchers.Main) { onDataReceived(articleList) }
        } catch (e: Exception) {
            Log.e("Application", e.toString())
            GlobalScope.launch(Dispatchers.Main) { onInternetError() }
            return false
        }
        return true
    }

    private fun onDataReceived(articleList: MutableList<Article>?) {
        newsActivityInterface?.showArticleList(articleList ?: mutableListOf())
        newsActivityInterface?.hideLoading()
    }

    private fun onInternetError() {
        newsActivityInterface?.showNotification(R.string.internet_error_text)
        newsActivityInterface?.hideLoading()
    }

    fun onViewDestroyed() {
        lastRequest?.let { newsActivityInterface?.saveRequest(it) }
    }

    fun onArticleItemDeleteClick(position: Int) {
        articleRemovePosition = position
        newsActivityInterface?.createConfirmationDialog(
            title = R.string.confirmation_dialog_title,
            description = "You are deleting article named " +
                    "${newsActivityInterface?.getArticle(position)?.title}. " +
                    "Please confirm your action.",
        )
    }

    fun unbind() {
        newsActivityInterface = null
    }

    fun bind(newsActivityInterface: NewsFragmentInterface) {
        this.newsActivityInterface = newsActivityInterface
    }

    fun onNewsItemRemoveActionConfirmed() {
        articleRemovePosition?.let { newsActivityInterface?.removeArticle(it) }
        newsActivityInterface?.hideConfirmationDialog()
    }

    fun onNewsItemRemoveActionDismissed() {
        newsActivityInterface?.hideConfirmationDialog()
    }

    companion object {
        private val NULL_REQUEST = null
    }
}