package com.example.newsapi_application.news

import android.os.Bundle
import android.text.Editable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.newsapi_application.R
import com.example.newsapi_application.databinding.NewsFragmentBinding
import com.example.newsapi_application.hideKeyboard
import com.example.newsapi_application.model.Article
import com.example.newsapi_application.news.widgets.ConfirmationDialog
import com.example.newsapi_application.news.widgets.RecyclerViewAdapter
import com.example.newsapi_application.system.PreferencesManager

class NewsFragment : Fragment(), NewsFragmentInterface,
    ConfirmationDialog.ConfirmationDialogCallback {
    var onAcceptButtonClickCallback: (() -> Unit)? = null
    var onDeclineButtonClickCallback: (() -> Unit)? = null
    private lateinit var binding: NewsFragmentBinding
    private lateinit var newsListAdapter: RecyclerViewAdapter
    private val newsPresenter = NewsPresenter(this)
    private val preferencesManager = PreferencesManager(activity)
    private var dialog: DialogFragment? = null


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(layoutInflater, R.layout.news_fragment, container, false)

        binding.findButton.setOnClickListener(this::findButtonClick)

        initRecyclerView()
        initNewsData(savedInstanceState)
        notifyPresenterAboutCreation(savedInstanceState)

        newsListAdapter = binding.newsList.adapter as RecyclerViewAdapter

        return binding.root
    }

    private fun initRecyclerView() {
        binding.newsList.apply {
            layoutManager = LinearLayoutManager(context)
            adapter =
                RecyclerViewAdapter(
                    mutableListOf()
                ) { item ->
                    newsPresenter.onArticleItemDeleteClick(item)
                }
        }
    }

    private fun initNewsData(savedInstanceState: Bundle?) {
        savedInstanceState?.run {
            binding.inputField.hint = null

            newsPresenter.bind(this@NewsFragment)
            getString(INPUT_STRING_PARAM)?.let {
                binding.inputField.text = Editable.Factory().newEditable(it)
            }
            getParcelableArray(ARTICLE_LIST_PARAM)?.let { array ->
                binding.newsList.adapter =
                    RecyclerViewAdapter(
                        array.toMutableList() as MutableList<Article>
                    ) {
                        onDeleteItemButtonClick(it)
                    }
            }
        }
    }

    private fun notifyPresenterAboutCreation(savedInstanceState: Bundle?) {
        if (savedInstanceState == null) {
            newsPresenter.onViewCreated()
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.apply {
            putString(INPUT_STRING_PARAM, binding.inputField.text.toString())
            putParcelableArray(
                ARTICLE_LIST_PARAM,
                newsListAdapter.dataList.toTypedArray()
            )
        }
        newsPresenter.unbind()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        newsPresenter.onViewDestroyed()
    }

    private fun findButtonClick(view: View) {
        newsPresenter.onFindButtonClick(binding.inputField.text.toString())
    }

    override fun showLoading() {
        binding.progressCircular.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        binding.progressCircular.visibility = View.INVISIBLE
    }

    override fun hideKeyboard() {
        activity?.hideKeyboard()
    }

    override fun showArticleList(articleList: MutableList<Article>) {
        newsListAdapter.dataList = articleList
        newsListAdapter.notifyDataSetChanged()
    }

    override fun showNotification(stringId: Int) {
        Toast.makeText(context, getText(stringId), Toast.LENGTH_SHORT).show()
    }

    override fun saveRequest(request: String) {
        preferencesManager.saveString(REQUEST_PREFERENCE, request)
    }

    override fun loadRequest(): String? =
        preferencesManager.loadString(REQUEST_PREFERENCE, null)

    override fun showTextInInputField(text: String) {
        binding.inputField.setText(text)
    }

    override fun createConfirmationDialog(
        title: Int?,
        description: String?,
        onAcceptButtonTitle: String?,
        onDeclineButtonTitle: String?
    ) {
        this.onAcceptButtonClickCallback = { newsPresenter.onNewsItemRemoveActionConfirmed() }
        this.onDeclineButtonClickCallback = { newsPresenter.onNewsItemRemoveActionDismissed() }
        dialog = ConfirmationDialog.newInstance(
            title?.let { resources.getString(it) },
            description,
            resources.getString(R.string.confirmation_dialog_confirm_button_text),
            resources.getString(R.string.confirmation_dialog_deny_button_text)
        ).let { dialog ->
            dialog.setStyle(DialogFragment.STYLE_NORMAL, R.style.ConfirmationDialog)
            dialog.setTargetFragment(this, 0)
            dialog.show(parentFragmentManager, null)
            dialog
        }
    }

    override fun hideConfirmationDialog() {
        dialog?.dismiss()
    }

    override fun getArticle(position: Int): Article =
        newsListAdapter.dataList[position]

    override fun removeArticle(position: Int) {
        newsListAdapter.dataList.removeAt(position)
        newsListAdapter.notifyItemRemoved(position)
    }

    private fun onDeleteItemButtonClick(item: Int) {
        newsPresenter.onArticleItemDeleteClick(item)
    }

    override fun onConfirmButtonClicked() {
        onAcceptButtonClickCallback?.invoke()
    }

    override fun onDeclineButtonClicked() {
        onDeclineButtonClickCallback?.invoke()
    }

    companion object {
        private const val INPUT_STRING_PARAM = "inputString"
        private const val ARTICLE_LIST_PARAM = "articles"
        private const val REQUEST_PREFERENCE = "request"

        fun newInstance() = NewsFragment()
    }
}