package com.example.newsapi_application.news

import com.example.newsapi_application.model.Article

interface NewsFragmentInterface {
    fun showLoading()

    fun hideLoading()

    fun hideKeyboard()

    fun showArticleList(articleList: MutableList<Article>)

    fun showNotification(stringId: Int)

    fun saveRequest(request: String)

    fun loadRequest(): String?

    fun showTextInInputField(text: String)

    fun createConfirmationDialog(
        title: Int? = null,
        description: String? = null,
        onAcceptButtonTitle: String? = null,
        onDeclineButtonTitle: String? = null
    )

    fun hideConfirmationDialog()

    fun getArticle(position: Int): Article

    fun removeArticle(position: Int)
}