package com.example.newsapi_application.news.widgets

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.DataBindingUtil
import com.example.newsapi_application.R
import com.example.newsapi_application.databinding.NewsTitleLayoutBinding
import com.squareup.picasso.Picasso

class NewsTitleView(context: Context, attributeSet: AttributeSet?) :
    ConstraintLayout(context, attributeSet) {
    constructor(context: Context) : this(context, null)

    private val binding: NewsTitleLayoutBinding =
        DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.news_title_layout,
            this,
            true
        )

    var title: String?
        get() = binding.textViewTitle.text.toString()
        set(value) {
            binding.textViewTitle.text = value
        }

    var urlToImage: String? = null
        set(value) {
            field = value
            loadImage(field)
        }

    @SuppressLint("NewApi")
    private fun loadImage(url: String?) {
        Picasso.get().isLoggingEnabled = true

        url?.let {
            Picasso.get()
                .load(it)
                .noPlaceholder()
                .into(binding.imageViewNewsImage)
        }
    }
}