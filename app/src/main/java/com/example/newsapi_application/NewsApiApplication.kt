package com.example.newsapi_application

import android.app.Application
import com.example.newsapi_application.di.ApplicationComponent
import com.example.newsapi_application.di.DaggerApplicationComponent
import com.example.newsapi_application.di.modules.ApplicationModule


class NewsApiApplication : Application() {
    lateinit var applicationComponent: ApplicationComponent

    override fun onCreate() {
        super.onCreate()
        initApplicationComponent()
        instance = this
    }

    private fun initApplicationComponent() {
        applicationComponent = DaggerApplicationComponent
            .builder()
            .applicationModule(ApplicationModule(this))
            .build()
    }

    companion object {
        lateinit var instance: NewsApiApplication
            private set
    }

}