package com.example.newsapi_application.di.modules

import android.app.Application
import com.example.newsapi_application.di.scopes.MapViewModelScope
import com.example.newsapi_application.map.MapInteractor
import com.example.newsapi_application.map.MapViewModel
import dagger.Module
import dagger.Provides

@Module
class MapViewModelModule {
    @MapViewModelScope
    @Provides
    fun getMapViewModel(
        application: Application,
        mapInteractor: MapInteractor
    ) = MapViewModel(application, mapInteractor)
}