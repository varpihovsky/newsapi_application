package com.example.newsapi_application.di.scopes

import javax.inject.Scope

@Scope
annotation class MapFragmentScope

@Scope
annotation class MapViewModelScope