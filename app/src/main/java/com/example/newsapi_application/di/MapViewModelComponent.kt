package com.example.newsapi_application.di

import com.example.newsapi_application.di.modules.MapViewModelModule
import com.example.newsapi_application.di.scopes.MapViewModelScope
import com.example.newsapi_application.map.MapViewModel
import dagger.Subcomponent

@MapViewModelScope
@Subcomponent(modules = [MapViewModelModule::class])
interface MapViewModelComponent {
    fun getMapViewModel(): MapViewModel
}