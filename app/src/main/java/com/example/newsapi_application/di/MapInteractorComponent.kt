package com.example.newsapi_application.di

import com.example.newsapi_application.di.modules.MapInteractorModule
import com.example.newsapi_application.di.modules.MapViewModelModule
import com.example.newsapi_application.di.scopes.MapFragmentScope
import dagger.Subcomponent

@MapFragmentScope
@Subcomponent(modules = [MapInteractorModule::class])
interface MapInteractorComponent {
    fun plus(mapViewModelModule: MapViewModelModule): MapViewModelComponent
}