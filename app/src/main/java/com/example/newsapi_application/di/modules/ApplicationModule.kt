package com.example.newsapi_application.di.modules

import android.app.Application
import dagger.Module
import dagger.Provides

@Module
class ApplicationModule(private val application: Application) {
    @Provides
    fun getApplication() = application

    @Provides
    fun getContext() = application.applicationContext
}