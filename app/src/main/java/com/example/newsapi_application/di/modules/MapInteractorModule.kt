package com.example.newsapi_application.di.modules

import android.app.Activity
import android.content.Context
import com.example.newsapi_application.di.scopes.MapFragmentScope
import com.example.newsapi_application.map.MapInteractor
import com.example.newsapi_application.map.PlaceSearcher
import com.example.newsapi_application.map.RouteBuilder
import com.example.newsapi_application.system.LocationProvider
import com.example.newsapi_application.system.PermissionsManager
import dagger.Module
import dagger.Provides

@Module
class MapInteractorModule(private val activity: Activity) {
    @MapFragmentScope
    @Provides
    fun getMapInteractor(
        placeSearcher: PlaceSearcher,
        routeBuilder: RouteBuilder,
        locationProvider: LocationProvider,
        permissionsManager: PermissionsManager
    ) = MapInteractor(placeSearcher, routeBuilder, locationProvider, permissionsManager)

    @MapFragmentScope
    @Provides
    fun getPlaceSearcher() = PlaceSearcher()

    @MapFragmentScope
    @Provides
    fun getRouteBuilder() = RouteBuilder()

    @MapFragmentScope
    @Provides
    fun getLocationProvider(context: Context) = LocationProvider(context)

    @MapFragmentScope
    @Provides
    fun getPermissionManager(activity: Activity) = PermissionsManager(activity)

    @MapFragmentScope
    @Provides
    fun getActivity() = activity
}