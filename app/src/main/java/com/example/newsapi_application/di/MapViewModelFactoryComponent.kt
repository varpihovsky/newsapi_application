package com.example.newsapi_application.di

import com.example.newsapi_application.di.modules.MapViewModelFactoryModule
import com.example.newsapi_application.di.scopes.MapFragmentScope
import com.example.newsapi_application.map.MapFragment
import dagger.Subcomponent

@MapFragmentScope
@Subcomponent(modules = [MapViewModelFactoryModule::class])
interface MapViewModelFactoryComponent {
    fun inject(mapFragment: MapFragment)
}