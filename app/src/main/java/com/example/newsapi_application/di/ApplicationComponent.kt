package com.example.newsapi_application.di

import android.app.Application
import android.content.Context
import com.example.newsapi_application.di.modules.ApplicationModule
import com.example.newsapi_application.di.modules.MapInteractorModule
import com.example.newsapi_application.di.modules.MapViewModelFactoryModule
import com.example.newsapi_application.di.scopes.ApplicationScope
import dagger.Component

@ApplicationScope
@Component(modules = [ApplicationModule::class])
interface ApplicationComponent {
    fun getApplication(): Application
    fun getContext(): Context
    fun plusMapViewModelFactory(mapViewModelFactoryModule: MapViewModelFactoryModule): MapViewModelFactoryComponent
    fun plusMapFragment(mapInteractorModule: MapInteractorModule): MapInteractorComponent
}