package com.example.newsapi_application.di.modules

import android.app.Application
import com.example.newsapi_application.di.MapInteractorComponent
import com.example.newsapi_application.di.scopes.MapFragmentScope
import com.example.newsapi_application.map.MapViewModelFactory
import dagger.Module
import dagger.Provides

@Module(includes = [ApplicationModule::class])
class MapViewModelFactoryModule(private val mapInteractorComponent: MapInteractorComponent) {
    @MapFragmentScope
    @Provides
    fun getFactory(application: Application, mapInteractorComponent: MapInteractorComponent) =
        MapViewModelFactory(application, mapInteractorComponent)

    @MapFragmentScope
    @Provides
    fun getMapInteractorComponent() = mapInteractorComponent
}