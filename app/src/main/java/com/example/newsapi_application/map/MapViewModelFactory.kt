package com.example.newsapi_application.map

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.newsapi_application.di.MapInteractorComponent
import com.example.newsapi_application.di.modules.MapViewModelModule
import javax.inject.Inject

class MapViewModelFactory @Inject constructor(
    application: Application,
    private val mapInteractorComponent: MapInteractorComponent
) :
    ViewModelProvider.AndroidViewModelFactory(application) {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return mapInteractorComponent.plus(MapViewModelModule()).getMapViewModel() as T
    }
}