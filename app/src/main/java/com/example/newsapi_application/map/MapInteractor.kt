package com.example.newsapi_application.map

import android.location.Location
import com.example.newsapi_application.NewsApiApplication
import com.example.newsapi_application.system.LocationProvider
import com.example.newsapi_application.system.PermissionsManager
import com.mapbox.api.directions.v5.models.DirectionsRoute
import com.mapbox.mapboxsdk.geometry.LatLng
import pub.devrel.easypermissions.EasyPermissions
import javax.inject.Inject

class MapInteractor @Inject constructor(
    private val placeSearcher: PlaceSearcher,
    private val routeBuilder: RouteBuilder,
    private val locationProvider: LocationProvider,
    private val permissionsManager: PermissionsManager
) {
    private lateinit var currentLocation: Location

    fun getCurrentLocation(block: (Location) -> Unit) {
        locationProvider.getCurrentLocationCallback {
            currentLocation = it
            block(it)
        }
    }

    fun isEqualToCurrentLocation(location: Location) =
        LatLng(currentLocation.latitude, currentLocation.longitude) != LatLng(
            location.latitude,
            location.longitude
        )

    fun findPlacesByCategory(
        category: String,
        currentMarkers: List<MarkerDTO>?,
        block: (filtered: List<LatLng>, unfiltered: List<MarkerDTO>) -> Unit
    ) {
        placeSearcher.findPlacesByCategoryCallback(category) {
            it.mapNotNull { marker -> marker.toMarkerDTO() }.let { list ->
                block(list.filter { marker -> currentMarkers?.contains(marker) == true }
                    .map { marker -> marker.latLng },
                    list
                )
            }
        }
    }


    fun buildRouteBlocking(from: LatLng, to: LatLng): DirectionsRoute? {
        return routeBuilder.buildRouteBlocking(from, to)
    }

    fun requestPermissions(vararg permissions: String) {
        permissionsManager.requestPermission(*permissions)
    }

    fun hasPermissions(permissions: Array<String>) = EasyPermissions.hasPermissions(
        NewsApiApplication.instance,
        *permissions
    )
}