package com.example.newsapi_application.map

import android.Manifest
import android.app.Application
import android.location.Location
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.newsapi_application.system.ViewModelEvent
import com.mapbox.api.directions.v5.models.DirectionsRoute
import com.mapbox.mapboxsdk.geometry.LatLng
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import javax.inject.Inject

class MapViewModel
@Inject constructor(
    application: Application,
    private val mapInteractor: MapInteractor
) : AndroidViewModel(application) {
    val data by lazy { Data() }

    private val route = MutableLiveData<DirectionsRoute>()
    private val markers = MutableLiveData<List<MarkerDTO>>()
    private val currentLocation = MutableLiveData<MarkerDTO>()
    private val moveCameraEvent = MutableLiveData<ViewModelEvent<LatLng>>()
    private val removeMarkerEvent = MutableLiveData<ViewModelEvent<List<LatLng>>>()

    inner class Data {
        val routeLiveData: LiveData<DirectionsRoute> = this@MapViewModel.route
        val markersLiveData: LiveData<List<MarkerDTO>> = this@MapViewModel.markers
        val currentLocationLiveData: LiveData<MarkerDTO> = this@MapViewModel.currentLocation
        val moveCameraEvent: LiveData<ViewModelEvent<LatLng>> = this@MapViewModel.moveCameraEvent
        val removeMarkersEvent: LiveData<ViewModelEvent<List<LatLng>>> =
            this@MapViewModel.removeMarkerEvent
    }

    fun onCurrentLocationHandle() {
        if (mapInteractor.hasPermissions(requiredPermissions)
        ) {
            onPermissionsGranted()
        } else {
            mapInteractor.requestPermissions(*requiredPermissions)
        }
    }

    fun onPermissionsGranted() {
        mapInteractor.getCurrentLocation { handleLocation(it) }
    }

    private fun handleLocation(location: Location) {
        viewModelScope.launch {
            if (!mapInteractor.isEqualToCurrentLocation(location)) {
                currentLocation.value =
                    MarkerDTO("Your Location", LatLng(location.latitude, location.longitude))
            }
            currentLocation.value?.let { moveCameraEvent.value = ViewModelEvent(it.latLng) }
        }
        putPlaces()
    }



    private fun putPlaces() {
        mapInteractor.findPlacesByCategory(CATEGORY, markers.value) { filtered, unfiltered ->
            removeMarkerEvent.value = ViewModelEvent(filtered)
            markers.value = unfiltered
        }
    }

    fun handleRoute(latLng: LatLng) {
        GlobalScope.launch(Dispatchers.IO) {
            currentLocation.value?.let {
                mapInteractor.buildRouteBlocking(it.latLng, latLng)
            }
        }
    }

    companion object {
        private const val CATEGORY = "restaurant"

        private val requiredPermissions = arrayOf(
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
        )

    }
}