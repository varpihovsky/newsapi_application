package com.example.newsapi_application.map

import com.mapbox.api.directions.v5.DirectionsCriteria
import com.mapbox.api.directions.v5.MapboxDirections
import com.mapbox.api.directions.v5.models.DirectionsRoute
import com.mapbox.geojson.Point
import com.mapbox.mapboxsdk.geometry.LatLng
import javax.inject.Inject

class RouteBuilder @Inject constructor() {
    fun buildRouteBlocking(from: LatLng, to: LatLng): DirectionsRoute? {
        return initDirectionsRequest(from, to).executeCall().body()?.routes()?.get(0)
    }

    private fun initDirectionsRequest(from: LatLng, to: LatLng) =
        MapboxDirections.builder()
            .accessToken(MAPBOX_ACCESS_TOKEN)
            .origin(Point.fromLngLat(from.longitude, from.latitude))
            .destination(Point.fromLngLat(to.longitude, to.latitude))
            .overview(DirectionsCriteria.OVERVIEW_FULL)
            .profile(DirectionsCriteria.PROFILE_DRIVING)
            .geometries(DirectionsCriteria.GEOMETRY_POLYLINE)
            .build()

    companion object {
        private const val MAPBOX_ACCESS_TOKEN =
            "sk.eyJ1IjoidmFycGlob3Zza3kiLCJhIjoiY2tuM2N2bGp5MDd0eDJ2bzBzZXp5a3ZkayJ9.pOYaC4p-9s-_r75j9-NHPw"
    }
}