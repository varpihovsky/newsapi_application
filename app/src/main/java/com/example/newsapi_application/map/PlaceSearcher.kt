package com.example.newsapi_application.map

import android.util.Log
import com.mapbox.search.*
import com.mapbox.search.result.SearchResult
import javax.inject.Inject

class PlaceSearcher @Inject constructor() : SearchCallback {
    private val categorySearchEngine = MapboxSearchSdk.createCategorySearchEngine()
    private lateinit var searchRequestTask: SearchRequestTask
    private var block: ((List<SearchResult>) -> Unit)? = null

    override fun onError(e: Exception) {
        Log.d("Application", "Search Error")
    }

    override fun onResults(results: List<SearchResult>, responseInfo: ResponseInfo) {
        Log.d("Application", results.toString())
        block?.invoke(results)
    }

    fun findPlacesByCategoryCallback(
        category: String,
        block: (List<SearchResult>) -> Unit
    ) {
        this.block = block

        val categorySearchOptions = CategorySearchOptions.Builder().limit(20).build()
        searchRequestTask = categorySearchEngine.search(category, categorySearchOptions, this)
    }
}