package com.example.newsapi_application.map

import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.search.result.SearchResult

data class MarkerDTO(var title: String, var latLng: LatLng)

fun SearchResult.toMarkerDTO(): MarkerDTO? {
    return coordinate?.let { MarkerDTO(name, LatLng(it.latitude(), it.longitude())) }
}