package com.example.newsapi_application.map

import android.app.Activity
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.newsapi_application.NewsApiApplication
import com.example.newsapi_application.R
import com.example.newsapi_application.databinding.MapFragmentLayoutBinding
import com.example.newsapi_application.di.modules.MapInteractorModule
import com.example.newsapi_application.di.modules.MapViewModelFactoryModule
import com.mapbox.api.directions.v5.models.DirectionsRoute
import com.mapbox.core.constants.Constants
import com.mapbox.geojson.Feature
import com.mapbox.geojson.LineString
import com.mapbox.mapboxsdk.Mapbox
import com.mapbox.mapboxsdk.annotations.Marker
import com.mapbox.mapboxsdk.annotations.MarkerOptions
import com.mapbox.mapboxsdk.camera.CameraPosition
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.maps.Style
import com.mapbox.mapboxsdk.style.layers.LineLayer
import com.mapbox.mapboxsdk.style.layers.Property.LINE_CAP_ROUND
import com.mapbox.mapboxsdk.style.layers.Property.LINE_JOIN_ROUND
import com.mapbox.mapboxsdk.style.layers.PropertyFactory.*
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource
import javax.inject.Inject

class MapFragment : Fragment() {
    @Inject
    lateinit var factory: MapViewModelFactory

    private lateinit var binding: MapFragmentLayoutBinding
    private lateinit var markers: List<Marker>

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return DataBindingUtil.inflate<MapFragmentLayoutBinding>(
            inflater,
            R.layout.map_fragment_layout,
            container,
            false
        ).let {
            binding = it
            binding.root
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.mapView.onCreate(savedInstanceState)

        injectFactory()

        binding.mapViewModel =
            ViewModelProvider(this, factory).get(MapViewModel::class.java)
        initMap()
        setupObservers()
    }

    private fun injectFactory() {
        NewsApiApplication.instance.applicationComponent.plusMapFragment(
            MapInteractorModule(activity as Activity)
        ).let {
            NewsApiApplication.instance.applicationComponent.plusMapViewModelFactory(
                MapViewModelFactoryModule(it)
            )
                .inject(this)
        }
    }

    private fun initMap() {
        binding.mapView.getMapAsync { mapboxMap ->
            mapboxMap.setStyle(initStyle())
            mapboxMap.setOnMarkerClickListener { marker ->
                showMarkerInfoWindow(marker)
                binding.mapViewModel?.handleRoute(marker.position)
                true
            }
            markers = mapboxMap.markers
        }
    }

    private fun initStyle() =
        Style.Builder().fromUri(Style.MAPBOX_STREETS)
            .withSource(GeoJsonSource(LINE_SOURCE_ID))
            .withLayer(initRouteLayer())

    private fun initRouteLayer() =
        LineLayer(LINE_LAYER_ID, LINE_SOURCE_ID).withProperties(
            lineWidth(NAVIGATION_LINE_WIDTH),
            lineOpacity(NAVIGATION_LINE_OPACITY),
            lineCap(LINE_CAP_ROUND),
            lineJoin(LINE_JOIN_ROUND),
            lineColor(Color.parseColor(getString(R.string.route_color)))
        )

    private fun setupObservers() {
        binding.mapViewModel?.data?.apply {
            markersLiveData.observe(viewLifecycleOwner) {
                showMarkers(it)
            }
            routeLiveData.observe(viewLifecycleOwner) {
                drawRoute(it)
            }
            currentLocationLiveData.observe(viewLifecycleOwner) {
                putMarker(it)
            }
            moveCameraEvent.observe(viewLifecycleOwner) {
                it.get()?.let { data -> moveCamera(data) }
            }
            removeMarkersEvent.observe(viewLifecycleOwner) {
                it.get()?.let { data -> removeMarkers(data) }
            }
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        context?.let {
            Mapbox.getInstance(
                it,
                resources.getString(R.string.mapbox_access_token)
            )
        }
    }

    override fun onStart() {
        super.onStart()
        binding.mapView.onStart()
    }

    override fun onPause() {
        super.onPause()
        binding.mapView.onPause()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        binding.mapView.onSaveInstanceState(outState)
    }

    override fun onStop() {
        super.onStop()
        binding.mapView.onStop()
    }

    override fun onResume() {
        super.onResume()
        binding.mapView.onResume()
    }

    override fun onDestroy() {
        super.onDestroy()
        binding.mapView.onDestroy()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding.mapView.onDestroy()
    }

    private fun removeMarkers(markers: List<LatLng>) {
        binding.mapView.getMapAsync { mapboxMap ->
            mapboxMap.markers.filter { markers.contains(it.position) }
                .forEach { mapboxMap.removeMarker(it) }
        }
    }

    private fun showMarkers(markers: List<MarkerDTO>) {
        markers.forEach { marker -> putMarker(marker) }
    }

    private fun putMarker(markerDTO: MarkerDTO) {
        binding.mapView.getMapAsync { mapboxMap ->
            mapboxMap.addMarker(
                MarkerOptions().title(markerDTO.title).position(markerDTO.latLng)
            )
        }
    }

    private fun drawRoute(directionsRoute: DirectionsRoute) {
        binding.mapView.getMapAsync { mapboxMap ->
            mapboxMap.getStyle { style -> putLineToSource(style, directionsRoute) }
        }
    }

    private fun putLineToSource(style: Style, directionsRoute: DirectionsRoute) {
        style.getSourceAs<GeoJsonSource>(LINE_SOURCE_ID)
            ?.setGeoJson(Feature.fromGeometry(directionsRouteToLineString(directionsRoute)))
    }

    private fun directionsRouteToLineString(directionsRoute: DirectionsRoute) =
        directionsRoute.geometry()?.let { geometry ->
            LineString.fromPolyline(
                geometry,
                Constants.PRECISION_5
            )
        }

    private fun showMarkerInfoWindow(marker: Marker) {
        binding.mapView.getMapAsync {
            it.selectMarker(marker)
        }
    }

    private fun moveCamera(position: LatLng) {
        binding.mapView.getMapAsync {
            val cameraPosition = CameraPosition.Builder()
                .target(position)
                .zoom(CAMERA_ZOOM)
                .tilt(CAMERA_TILT)
                .build()

            it.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
        }
    }

    fun notifyPermissionsGranted() {
        binding.mapViewModel?.onPermissionsGranted()
    }

    companion object {
        private const val LINE_SOURCE_ID = "source"
        private const val LINE_LAYER_ID = "layer"

        private const val NAVIGATION_LINE_WIDTH = 6f
        private const val NAVIGATION_LINE_OPACITY = .8f

        private const val CAMERA_ZOOM = 10.0
        private const val CAMERA_TILT = 30.0
    }
}